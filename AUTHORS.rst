=======
Credits
=======

Development
----------------

* S.P. Mohanty <mohanty@aicrowd.com>

* G Spigler <giacomo.spigler@gmail.com>

* A Egli <adrian.egli@sbb.ch>

* E Nygren <erik.nygren@sbb.ch>

* Ch. Eichenberger <christian.markus.eichenberger@sbb.ch>

* Mattias Ljungström

Contributors
------------

None yet. Why not be the first?
