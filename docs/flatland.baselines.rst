flatland.baselines package
==========================

Submodules
----------

flatland.baselines.dueling\_double\_dqn module
----------------------------------------------

.. automodule:: flatland.baselines.dueling_double_dqn
    :members:
    :undoc-members:
    :show-inheritance:

flatland.baselines.model module
-------------------------------

.. automodule:: flatland.baselines.model
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: flatland.baselines
    :members:
    :undoc-members:
    :show-inheritance:
