import random

import numpy as np

from examples.demo import Demo

random.seed(1)
np.random.seed(1)

if __name__ == "__main__":
    Demo.run_example_flatland_000()
